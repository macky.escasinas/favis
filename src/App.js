
import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import {

  MDBContainer
} from 'mdb-react-ui-kit';


import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import {Container}  from 'react-bootstrap';

import HostCar from "./pages/HostCar";
import AffiliatesCar from "./pages/AffiliatesCar";
import CoHost from "./pages/CoHost";
import CarDetails from "./pages/CardDetails";
import ToroHost from "./pages/ToroHost";
/*import CarList from "./pages/CarList";*/
import Footer from "./components/Footer";
function App() {
  return (
   <div>
     <Router>
       <AppNavbar />
       <div>
                
              <Routes>
                <Route exact path ="/" element={<Home />} />
                 <Route exact path ="/affiliates" element={<AffiliatesCar />} />
                 
                   <Route exact path ="/hostcar" element={<HostCar />} />
                   <Route exact path ="/co-host" element={<CoHost />} />
                  
               <Route exact path ="/car-details" element={<CarDetails />} />
      {/*          <Route exact path ="/carlist" element={<CarList />} />*/}
                   <Route exact path ="/toro-profile" element={<ToroHost />} />
              </Routes>
          </div>
          <Footer />
        </Router>

   </div>
  );
}

export default App;
