import '../App.css';
import React, { useState } from 'react';
import { Link } from "react-router-dom";
import {
  MDBNavbar,
  MDBContainer,
  MDBIcon,
  MDBNavbarNav,
  MDBNavbarItem,
  MDBNavbarLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBDropdown,
  MDBDropdownMenu,
  MDBDropdownToggle,
  MDBDropdownItem,
  MDBNavbarBrand,
  MDBAccordion,MDBAccordionItem
} from 'mdb-react-ui-kit';
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {Offcanvas, Image,Container}   from 'react-bootstrap';

export default function App() {
  const [showNavRight, setShowNavRight] = useState(false);
   const [showNavColor, setShowNavColor] = useState(false);
  const [showNavColorSecond, setShowNavColorSecond] = useState(false);
  const [showNavColorThird, setShowNavColorThird] = useState(false);

  const [show, setShow] = useState(false);


  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
     <Offcanvas show={show} onHide={handleClose}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Register As Host</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
       <center> <Image src="./main-logo.png" rounded /></center>
           <FloatingLabel
        controlId="floatingInput"
        label="Email address"
        className="mb-3"
      >
        <Form.Control type="email" placeholder="name@example.com" />
      </FloatingLabel>
      <FloatingLabel controlId="floatingPassword" label="Password">
        <Form.Control  className="mb-3" type="password" placeholder="Password" />
      </FloatingLabel>
      <center>
        <Button variant="primary">Register</Button>{' '}
      </center>      
        </Offcanvas.Body>
      </Offcanvas>

      <Offcanvas show={show} onHide={handleClose}>

        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Register As Host</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
       <center> <Image src="./main-logo.png" rounded /></center>
        




           <FloatingLabel
        controlId="floatingInput"
        label="Email address"
        className="mb-3"
      >


        <Form.Control type="email" placeholder="name@example.com" />
      </FloatingLabel>
      <FloatingLabel controlId="floatingPassword" label="Password">
        <Form.Control  className="mb-3" type="password" placeholder="Password" />
      </FloatingLabel>
      <center>
        <Button variant="primary" className="mb-3">Register</Button>{' '}
      </center>    


      <h5 className="mb-3 text-center">Rental Management Services fees</h5>
         <MDBAccordion initialActive={0}>
      <MDBAccordionItem collapseId={1} headerTitle={<>
      <MDBIcon fas icon="question-circle" /> &nbsp;Item A</>}>
        The Favis shall receive 30% of the rental income if the Vehicle is rented at less than $100 per day.
      </MDBAccordionItem>
      <MDBAccordionItem collapseId={2} headerTitle={<>
      <MDBIcon fas icon="question-circle" /> &nbsp;Item B</>}>
        The Favis shall receive 25% of the rental income if the Vehicle is rented at $100 or more per day.
      </MDBAccordionItem>
       <MDBAccordionItem collapseId={3} headerTitle={<>
      <MDBIcon fas icon="question-circle" /> &nbsp;Item C</>}>
        The Favis shall receive 25% of the rental income if the Vehicle Owner owns 12 or more vehicles under The Favis' management program.
      </MDBAccordionItem>
       <MDBAccordionItem collapseId={4} headerTitle={<>
      <MDBIcon fas icon="question-circle" /> &nbsp;Item D</>}>
        The Favis shall receive 20 to 25% of the rental income if the Vehicle Owner owns 12 or more vehicles under The Favis' management program and some of the Vehicle are rented at $100 or more per day...
      </MDBAccordionItem>


    </MDBAccordion>  
        </Offcanvas.Body>
      </Offcanvas>
      <Container fluid>
      </Container>


      <MDBNavbar expand='lg' dark bgColor='dark'  >
        <MDBContainer fluid>
          <MDBNavbarBrand href='/'>
           <img
              src='./logo-white.png'
              height='50'
              alt=''
              loading='lazy'
            />
            
           </MDBNavbarBrand>
           <MDBNavbarToggler
          type='button'
          data-target='#navbarRightAlignExample'
          aria-controls='navbarRightAlignExample'
          aria-expanded='false'
          aria-label='Toggle navigation'
          onClick={() => setShowNavRight(!showNavRight)}
        >
          <MDBIcon icon='bars' fas />
        </MDBNavbarToggler>

        <MDBCollapse navbar show={showNavRight}>
          <MDBNavbarNav right fullWidth={false} className='mb-2 mb-lg-0'>
            <MDBNavbarItem>
              <MDBNavbarLink href="/" className="nav-style fw-bolder">Home</MDBNavbarLink>
               
            </MDBNavbarItem>
             <MDBNavbarItem>
              <MDBDropdown>
                <MDBDropdownToggle tag='a' className='nav-link fw-bolder' role='button'>
                  Services
                </MDBDropdownToggle>
                <MDBDropdownMenu>
                  <MDBDropdownItem link href="#" className="nav-style">Car Rental</MDBDropdownItem>
                  <MDBDropdownItem link href="/toro-profile" target="_blank" className="nav-style">Rent Now On Turo</MDBDropdownItem>
                
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavbarItem>


            
           {/*  <MDBNavbarItem>
              <MDBNavbarLink href="#" className="nav-style fw-bolder">Contact Us</MDBNavbarLink>
            </MDBNavbarItem>*/}
             <MDBNavbarItem>
            <MDBNavbarLink className="nav-style fw-bolder" href="/co-host">Become Co-Host</MDBNavbarLink>
             </MDBNavbarItem>
              <MDBNavbarItem>
            <MDBNavbarLink onClick={handleShow} className="nav-style fw-bolder">Login</MDBNavbarLink>
             </MDBNavbarItem>

          </MDBNavbarNav>
        </MDBCollapse>
        </MDBContainer>
      </MDBNavbar>
    </>
  );
}