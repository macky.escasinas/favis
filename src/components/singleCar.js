import React from 'react';
import Col from 'react-bootstrap/Col';
import {
  MDBCard,
  MDBCardImage,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBCardHeader,
  MDBIcon,MDBCardLink,MDBListGroupItem,MDBListGroup
} from 'mdb-react-ui-kit';

import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';
import {useEffect, useState} from "react";

export default function SingleCar({carProp}) {

const {name, brand, location, description, userId,price,productImage,type,turoLink} = carProp;
     let carType = "";
	if(userId=="host"){
      carType="Host";
  }else{
      carType="Affliates";
  }

const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
     <>
    
      <Col md={3} className="mb-3">

       <MDBCard>
   
      <MDBCardBody>

        <div className="mb-4">
                        <img className="img-fluid p-1 card-image" src={`${productImage}`}  alt="" />
                        <MDBBtn outline className='mt-3' size='sm' color='secondary' rounded >
                               2015
                             </MDBBtn>
                        <h5 className="text-capitalize book-now mt-3">{name}</h5>
                     
                        <MDBCardLink className="link-price"><h4 className="price-style book-now">${price}/Day</h4></MDBCardLink>
                           <hr/>
                            <div className="d-flex justify-content-evenly">
                            <div className="px-2">
                                <img className="img-fluid change-icon-size" src='/icons/meter.png'  alt="" />
                                <span> 2015</span>
                            </div>
                            <div className="px-2 border-left border-right">
                               <img className="img-fluid change-icon-size" src='/icons/gear-shift.png'  alt="" />
                                <span> AUTO</span>
                            </div>
                            <div className="px-2">
                               <img className="img-fluid change-icon-size" src='/icons/gas.png'  alt="" />
                                <span> 25K</span>
                            </div>
                        </div>

                  
                      
                    </div>
        <div className="d-grid gap-2">
        <Button className="btn-rent"  size="lg" target="_blank" href={turoLink}>
            Book Now
        </Button>
        </div>
      </MDBCardBody>
    </MDBCard>
     
       

</Col>
       
     
</>
  );
}