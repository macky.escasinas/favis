import React from 'react';
import {useEffect, useState} from "react";
import SingleCar from "../components/singleCar";

import {Card,Form,Container,Row, Col,Button}  from 'react-bootstrap';
import {
  MDBCard,
  MDBCardImage,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBRow,
  MDBCol,MDBContainer,MDBIcon,MDBBtn,MDBCardLink
} from 'mdb-react-ui-kit';


export default function ToroHost() {
  const [cars, setCars] = useState([]);

  useEffect(() =>{
     fetch(`https://renting-api.onrender.com/cars/viewall-active-car`)
        .then(res => res.json())
        .then(data => {
            setCars(data.map(car =>{
                return(
                    <SingleCar key={car._id} carProp={car} />
                )
            }))
        })


        

              
        
    },[])


  return (
    <>

   
    <div className="car-list-banner-container d-flex justify-content-center align-items-center flex-column-reverse flex-row flex-lg-column flex-xxl-column flex-md-column ">
        <div className="p-2 car-list-banner  "><center><h3>Maximize Your Profit</h3>
            <h1>Earn by Becoming a co-host</h1>
        </center>
       </div>
        <div className="p-2"><img src='./multiple-car.png'  className='img-fluid img-banner-carlist' alt='...' /></div>

      </div>
<MDBContainer fluid> 

<Container className="rental-management-container">

    <MDBContainer fluid id="" >
      <center><img className="img-fluid" src='/main-logo.png'  alt="" /><h2>Favis Car Rental <br/> Vehicle Rental Management Services</h2></center>
    
         
     
   </MDBContainer>
</  Container>
   
 
<Container className="car-container">
 <div class="d-inline-flex text-dark text-center justify-content-center">
             <h2 class="text-uppercase text-body m-0">Rental Management Fee's</h2>

         
        </div>
<MDBRow className='row-cols-1 row-cols-md-3 row-cols-sm-1 g-3 justify-content-center'>
<Col md={3} className="mb-3">
    <MDBCard>
      <MDBCardBody>
        <div className="mb-4">
        <h1 className="text-capitalize book-now mt-3 text-center">30%</h1>             
            <MDBCardLink className="link-price"><h4 className="price-style book-now"></h4></MDBCardLink>
            <hr/>
            <div className="d-flex justify-content-evenly">
                The Favis shall receive 30% of the rental income if the
                Vehicle is rented at
                less than $100 per day.
            </div>
                    </div>
        <div className="d-grid gap-2">
        <Button className="btn-warning"  size="lg" target="_blank" >
            Sign Up Now
        </Button>
        </div>
      </MDBCardBody>
    </MDBCard>
</Col>
<Col md={3} className="mb-3">
    <MDBCard>
      <MDBCardBody>
        <div className="mb-4">
        <h1 className="text-capitalize book-now mt-3 text-center">25%</h1>             
            <MDBCardLink className="link-price"><h4 className="price-style book-now"></h4></MDBCardLink>
            <hr/>
            <div className="d-flex justify-content-evenly">
                The Favis shall receive 25% of the rental income if the
                Vehicle is rented at
                $100 or more per day.
            </div>
                    </div>
        <div className="d-grid gap-2">
        <Button className="btn-warning"  size="lg" target="_blank" >
            Sign Up Now
        </Button>
        </div>
      </MDBCardBody>
    </MDBCard>
</Col>
<Col md={3} className="mb-3">
    <MDBCard className="pricing-card">
      <MDBCardBody>
        <div className="mb-4">
        <h1 className="text-capitalize book-now mt-3 text-center">25%</h1>             
            <MDBCardLink className="link-price"><h4 className="price-style book-now"></h4></MDBCardLink>
            <hr/>
            <div className="d-flex justify-content-evenly">
                The Favis shall receive 25% of the rental income if the
                Vehicle Owner
                owns 12 or more vehicles under The Favis' management
                program.
            </div>
                    </div>
        <div className="d-grid gap-2">
        <Button className="btn-warning"  size="lg" target="_blank" >
            Sign Up Now
        </Button>
        </div>
      </MDBCardBody>
    </MDBCard>
</Col>
<Col md={3} className="mb-3">
    <MDBCard>
      <MDBCardBody>
        <div className="mb-4">
        <h1 className="text-capitalize book-now mt-3 text-center">20-25%</h1>             
            <MDBCardLink className="link-price"><h4 className="price-style book-now"></h4></MDBCardLink>
            <hr/>
            <div className="d-flex justify-content-evenly">
              The Favis shall receive 20 to 25% of the rental income if
              the Vehicle
              Owner owns 12 or more vehicles under The Favis'
              management program
              and some of the Vehicle are rented at $100 or more per
              day...
            </div>
                    </div>
        <div className="d-grid gap-2">
        <Button className="btn-warning"  size="lg" target="_blank" >
            Sign Up Now
        </Button>
        </div>
      </MDBCardBody>
    </MDBCard>
</Col>
     </MDBRow>
</Container>


    </MDBContainer>

    </>
  );
}