import React from 'react';
import {useEffect, useState} from "react";
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {
  MDBBtn,
  MDBRow,
  MDBCol,MDBContainer,
    MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,MDBIcon,MDBCardImage,
  MDBCarousel,
  MDBCarouselItem,
} from 'mdb-react-ui-kit';
import AppNavbar from "../components/AppNavbar";

import SingleCar from "../components/singleCar";
export default function Home() {
  const [cars, setCars] = useState([]);
   useEffect(() =>{
     fetch(`https://renting-api.onrender.com/cars/viewall-active-car`)
        .then(res => res.json())
        .then(data => {
            setCars(data.map(car =>{
                return(
                    <SingleCar key={car._id} carProp={car} />
                )
            }))
        })
    },[])

  return (
    <>



 
      <MDBCarousel showIndicators showControls fade className="test">
        <MDBCarouselItem
        className='w-100 d-block'
        itemId={1}
        src='/carousel-1.jpg'
        alt='...'
      > 
                    <div className="pt-5 carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div className="p-3" >
                            <h4 className="text-white text-uppercase mb-md-3">Profit With Your Car</h4>
                            <h1 className="display-1 text-white mb-md-4">Earn passive income with your car</h1>
                            <a href="/co-host" className="btn btn-warning py-md-3 px-md-5 mt-2 warning">Learn More</a>
                        </div>
                    </div>
              

      </MDBCarouselItem>

      <MDBCarouselItem
        className='w-100 d-block'
        itemId={2}
        src='/carousel-2.jpg'
        alt='...'
      >
        <div className="carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div className="p-3" >
                            <h4 className="text-white text-uppercase mb-md-3">Co-Host</h4>
                            <h6 className="display-1 text-white mb-md-4">Profit More With minimal time and effort.</h6>
                            <a href="/co-host" className="btn btn-warning py-md-3 px-md-5 mt-2 ">Learn More</a>
                        </div>
                    </div>
      </MDBCarouselItem>
    </MDBCarousel>
 
  <br/>
  <MDBContainer fluid className="pt-5"> 
  <Container className="aboutus-container" id="about">
        <center><span className="text-center pt-5 pb-5 section-header"><b>About Us</b></span></center>
        <div className='d-flex justify-content-evenly flex-lg-row flex-column'>
            <div className="p-2"><center><img className="img-fluid" src='/main-logo.png'  alt="" /><h2>Favis Car Rental <br/> Vehicle Rental Management Services</h2></center></div>
            <div className="p-2"><h2>Our Services</h2>
                <h6><img className="img-fluid change-icon-size" src='/icons/check.png'  alt="" />Take the hassle out of car rental with our full
management services.</h6>
                <h6><img className="img-fluid change-icon-size" src='/icons/check.png'  alt="" />Maximize your rental potential with our expert
management team.</h6>
                <h6><img className="img-fluid change-icon-size" src='/icons/check.png'  alt="" />Let us take the wheel - full car rental management
services for busy car
owners</h6>
 <h6><img className="img-fluid change-icon-size" src='/icons/check.png'  alt="" />Relax, we've got you covered - comprehensive
management services for
hassle-free car rental.</h6>
 <h6><img className="img-fluid change-icon-size" src='/icons/check.png'  alt="" />Don't let time hold you back - our management services
keep your rental
activities running smoothly</h6>

            </div>
        </div>

</Container>
</MDBContainer> 



   <MDBContainer fluid className="fuel-container">
    <center> <span className="section-header  text-center"><b>What We Offer</b></span>
    <h2>How We Help?</h2></center>
     <div class="container-fluid">
         <div class="container">
             <div class="row mx-0">
                 <div class="col-lg-6 px-0">
                     <div class="px-5 bg-secondary d-flex align-items-center justify-content-between container-banner" >
                         <img class="img-fluid flex-shrink-0 ml-n5 w-50 mr-4" src="/banner-left.png" alt=""/>
                         <div class="text-right">
                             <h2 class="text-uppercase text-light mb-3">Rental Management Services</h2>
         
                             <a class="btn btn-warning py-2 px-4" href="/co-host">Become co-host</a>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-6 px-0">
                     <div class="px-5 bg-dark d-flex align-items-center justify-content-between container-banner" >
                         <div class="text-left">
                             <h2 class="text-uppercase text-light mb-3">Car Rental Promotion in TURO</h2>

                             <a class="btn btn-warning py-2 px-4" href="/toro-profile">View Vehicle</a>
                         </div>
                         <img class="img-fluid flex-shrink-0 mr-n5 w-50 ml-4" src="/banner-right.png" alt=""/>
                     </div>
                 </div>
             </div>
         </div>
     </div>
    </MDBContainer>



     <Container>
      <Row >
       
<div class="container-fluid py-5">
        <div class="container pb-3">
              <center> <span className="section-header  text-center"><b>Work Flow</b></span>
              <h2>How We Works</h2></center>

              <div class="d-flex flex-column justify-content-evenly mb-3 flex-lg-row">
               <div class="text-center"><img className="img-fluid icon-works" src='/icons/insurance.png'  alt="" />
   <br/><b>Insurance<br/></b><p class="text-wrap">The Vehicle Owner shall maintain<br/>
the required insurance on their Vehicle at all times, while
Turo provides <br/>insurance coverage for the Vehicle during the
rental period.</p></div>
  <div class="text-center"><img className="img-fluid icon-works" src='/icons/management.png'  alt="" />
  <br/><b>Rental Management Services</b>
  <p>Rental Management Services fees</p></div>
  <div class="text-center"><img className="img-fluid icon-works" src='/icons/marketing.png'  alt="" />
  <br/><b>Advertising and Promotion</b><br/>
  <p>The Favis shall advertise <br/>the
Vehicle on the Turo platform to maximize rental potential.</p></div>
  <div class="text-center"><img className="img-fluid icon-works" src='/icons/maintenance.png'  alt="" />
  <br/><b>Maintenance and Cleaning</b><br/><p>The Favis shall ensure that  <br/>the
Vehicle is clean and well-maintained before each rental.</p></div>
   
</div>
            
        
        </div>
    </div>
      </Row>
     

    </Container>



    </>
  );
}